#install.packages("readxl")
library(readxl)

input <- read_excel("data.xlsx")
#sink(file = "cor.txt", append = FALSE, type = c("output", "message"), split = FALSE)
m <- cor(input)

#install.packages("corrplot")
library('corrplot')
corrplot(m, method = "circle")

#install.packages("tree")
#library(tree)

#result_tree <- tree(Reopened~., input)
#summary(result_tree)
#plot(result_tree)
#text(result_tree)
#result <- cv.tree(result_tree, FUN=prune.misclass, K=10)
#plot(result$size, result$dev, type="b")

#######################
#Create buckets for 10 fold cross validation
#######################
input[,1] <- lapply(input[,1], factor) 
kfold <-function(inputData, k){
  n <- nrow(inputData)
  part <- 1+n%/%k
  set.seed(5)
  rank <- rank(runif(n))
  bluckets <- (rank)%/%part + 1
  bluckets <- as.factor(bluckets)
}

K <- 10
buckets <- kfold(input, K)
print(summary(buckets))
print(buckets)

#############################
#Apply predictions for 10 fold cross validation buckets
#############################
max1 <-function(i) {
  if(i > 1){
    return(1)
  } else {
    return(i)
  }
}

createPrediction <- function(predictor, cutOff) {
  sum <- matrix(0,nrow=2, ncol=2)
  for(k in 1:K){
    tree <- predictor(input[buckets!=k,])
    pred <- predict(tree, newdata=input[buckets==k,], type="prob")[,1] %/% cutOff
    pred <- sapply(pred, max1)
    cm <- table(input$Reopened[buckets==k], factor(pred, c(0,1)))
    sum <- cm + sum
  }
  row.names(sum) <- c("actual 0:","actual 1:")
  return(sum)
}

################################
# Prediction assessment
################################
assessReponesPrediction <- function(cm){
  sum_tt <- cm[1,1]
  sum_tn <- cm[2,1] 
  sum_nt <- cm[1,2] 
  sum_nn <- cm[2,2]
  n_ok <- sum_tt + sum_nt
  n_bad <- sum_nn + sum_tn
  n <- n_ok+n_bad
  c_t <- 2
  c_pr <- c_t * 10 
  testing_cost <- n*c_t
  P_failure <- sum_tn/n
  P_success <- sum_tt/n
  TG <- n_ok * P_success * c_t - n_bad * P_failure * (c_pr-c_t)
  percent_of_saving <- 100 * TG/(n*c_t)
  result <- c(TG, percent_of_saving)
  names(result) <- c("Total Gain", "% of Savings")
  return(result)
}

#############################
#Try Recursive Partitioning and Regression Trees
#############################
library(rpart)
applyRpart <- function(trainingData){
  return(rpart(Reopened~., data=trainingData, method="class"))
}

cm <- createPrediction(applyRpart, 0.15)
assessReponesPrediction(cm)
cm <- createPrediction(applyRpart, 0.5)
assessReponesPrediction(cm)

