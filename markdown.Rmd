---
title: "10-fold cross validation for reexecuting repones prediction"
author: "Marian Jureczko"
date: "October 3, 2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Prediction execution and evaluation functions

Train and execute predictor, the predictior is function that should be provided as a parameter:
```{r}
max1 <-function(i) {
  if(i > 1){
    return(1)
  } else {
    return(i)
  }
}

createPrediction <- function(predictor, cutOff, predictType) {
  sum <- matrix(0,nrow=2, ncol=2)
  for(k in 1:K){
    tree <- predictor(input[buckets!=k,])
    pred <- predict(tree, newdata=input[buckets==k,], type=predictType)
    if(is.matrix(pred)) {
      pred <- pred[,1]
    }
    pred <- pred %/% cutOff
    pred <- sapply(pred, max1)
    cm <- table(input$Reopened[buckets==k], factor(pred, c(0,1)))
    sum <- cm + sum
  }
  row.names(sum) <- c("actual 0:","actual 1:")
  return(sum)
}
```

Prediction assessment according to the method suggested in our paper:
```{r}
assessReponesPrediction <- function(cm){
  sum_tt <- cm[1,1]
  sum_tn <- cm[2,1] 
  sum_nt <- cm[1,2] 
  sum_nn <- cm[2,2]
  n_ok <- sum_tt + sum_nt
  n_bad <- sum_nn + sum_tn
  n <- n_ok+n_bad
  c_t <- 2
  c_pr <- c_t * 10 
  testing_cost <- n*c_t
  P_failure <- sum_tn/n
  P_success <- sum_tt/n
  TG <- n_ok * P_success * c_t - n_bad * P_failure * (c_pr-c_t)
  percent_of_saving <- 100 * TG/(n*c_t)
  result <- c(TG, percent_of_saving)
  names(result) <- c("Total Gain", "% of Savings")
  return(result)
}
```

## Load and prepare data

Load data from XSLX file:
```{r}
#install.packages("readxl")
library(readxl)
input <- read_excel("data.xlsx")
```

Let us take a look at the correlations.
```{r}
m <- cor(input)
#install.packages("corrplot")
library('corrplot')
corrplot(m, method = "circle")
```

The orginal data set contains so large number of independent variables that it is hard to visualise it.
Here we have a reduced set that is employed in the further analysis.
Independen varaibles were removed when correlated with each other or has close to no correlation with the dependant variable.

It could be also interesting to take a look at the distribution of reopens:
```{r}
table(input$Reopened)
```
Please note that 1 stands for reopened and 0 for correct fixed.

In order to conduct 10-fold cross-validation the input data is split into 10 buckets:
```{r}
input[,1] <- lapply(input[,1], factor) 
kfold <-function(inputData, k){
  n <- nrow(inputData)
  part <- 1+n%/%k
  set.seed(5)
  rank <- rank(runif(n))
  bluckets <- (rank)%/%part + 1
  bluckets <- as.factor(bluckets)
}

K <- 10
buckets <- kfold(input, K)
print(summary(buckets))
```

## Predictors evaluation

### Recursive Partitioning and Regression Trees

This is one of the two predictors used in the orginal study. The other one (i.e. CHAID) is in R not applicable for numeric data, namely works only with categorical variables.
The predictor function is defined as follows:
``` {r}
library(rpart)
applyRpart <- function(trainingData){
  return(rpart(Reopened~., data=trainingData, method="class"))
}
```

Result for cut off equal 0.5:
``` {r}
cm <- createPrediction(applyRpart, 0.5, "prob")
assessReponesPrediction(cm)
```

Result for cut off equal 0.15 (decreased probability for predicting reopens as correct fixes to the level used in our experiments):
``` {r}
cm <- createPrediction(applyRpart, 0.15, "prob")
assessReponesPrediction(cm)
```

### Logistic regression

The predictor function is defined as follows:
```{r}
applyGlm <- function(trainingData){
  return(glm(formula = Reopened ~ ., family = binomial(link = "logit"),  data = trainingData))
}
```
Result for cut off equal 0.5:
```{r}
cm <- createPrediction(applyGlm, 0.5, 'response')
assessReponesPrediction(cm)
```

Result for cut off equal 0.15:
```{r}
cm <- createPrediction(applyGlm, 0.15, 'response')
assessReponesPrediction(cm)
```